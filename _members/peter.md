---
name: Peter Maydell
role: Emulation Specialist - Linaro
---
Peter works for ARM, but has been seconded into Linaro since 2011 to
handle all things ARM in QEMU, including CPU architecture emulation,
support for KVM virtualization on ARM servers, and herding an
ever-increasing number of board, SoC and device models. Before that he
worked for a small company on an embedded OS and JITting Java virtual
machine; watching a decade of work vanish when the company went under was
a compelling argument for the merits of working on open source codebases.
