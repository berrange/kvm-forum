---
dates: "October 24-26, 2018"
place: "Edinburgh, Scotland"
layout: archive
sponsors:
  - type: "Platinum Sponsor"
    who: ["Google Cloud", "Huawei", "Red Hat"]
  - type: "Gold Sponsors"
    who: ["IBM", "Intel"]
---
The schedule and slides are available on [linux-kvm.org](https://linux-kvm.org/page/KVM_Forum_2018).

Videos are available on [YouTube](https://www.youtube.com/playlist?list=PLW3ep1uCIRfylnZq66q1mPdZhSoGPJgTT).

# Pictures

* [Official pictures from the Linux Foundation](https://www.flickr.com/photos/linuxfoundation/albums/72157699759314672)
* [Group photo](https://live.staticflickr.com/1963/43819378880_6d4e2d149c_h.jpg).
