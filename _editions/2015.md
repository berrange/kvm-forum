---
dates: "August 19-21, 2015"
place: "Seattle, WA"
layout: archive
sponsors:
  - type: "Gold Sponsors"
    who: ["IBM", "Red Hat (old logo)"]
  - type: "Silver Sponsor"
    who: ["HPE", "Proxmox"]
  - type: "Speaker Reception"
    who: ["OVA"]
---
The schedule and slides are available on [linux-kvm.org](https://linux-kvm.org/page/KVM_Forum_2015).

Videos are available on [YouTube](https://www.youtube.com/playlist?list=PLW3ep1uCIRfyLNSu708gWG7uvqlolk0ep).

# Pictures

* [Official pictures from the Linux Foundation](https://www.flickr.com/photos/linuxfoundation/albums/72157655545164444)

# Blogs

* [Two hypervisors, one great collaboration](https://www.redhat.com/en/blog/two-hypervisors-one-great-collaboration)
