---
title: Sponsoring KVM Forum
layout: page
permalink: /sponsorship/
---
{% include relative_root.html %}

Sponsoring KVM Forum helps bringing together a diverse community of
developers for Linux-based virtualization and emulation techologies,
including KVM, QEMU, VFIO, virtio and more.

Sponsorships help making the conference successful and affordable,
and demonstrate support for the work of the open source virtualization
community.

If you are interested in sponsoring KVM Forum, please contact the organizers
at [kvm-forum-2024-pc@redhat.com](mailto:kvm-forum-2024-pc@redhat.com).

{% include sponsors.html %}
